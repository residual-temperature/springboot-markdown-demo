package com.yuwen.service;

import com.yuwen.pojo.Article;

import javax.servlet.http.HttpServletRequest;

public interface ArticleService {
    //添加文章
    public boolean addArticle(String content, HttpServletRequest request);
    //根据文章id查询文章
    public Article findById();
}
