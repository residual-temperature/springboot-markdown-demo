package com.yuwen.service.impl;

import com.yuwen.mapper.ArticleMapper;
import com.yuwen.pojo.Article;
import com.yuwen.service.ArticleService;
import com.yuwen.utils.CopyImg;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@Service
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    /**
     * 添加文章
     * @param content 文章的内容（富文本框中的值）
     * @param request request
     * @return 添加文章成功是否成功
     */
    @Override
    public boolean addArticle(String content,HttpServletRequest request){
        Article article = new Article();
        article.setId(1); //因为没有传入id进来，所以默认设置文章id为1
        try {
            String s = CopyImg.copyImg(content, request); //通过工具类将文章中的图片保存到要保存的地址方便下次回显
            article.setContent(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return articleMapper.addArticle(article);
    }

    /**
     * 根据id查询文章
     * @return 查询的文章
     */
    @Override
    public Article findById() {
        return articleMapper.findArticleById();
    }
}
