package com.yuwen.mapper;

import com.yuwen.pojo.Article;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArticleMapper{
    //添加文章
    public boolean addArticle(Article article);
    //根据文章id查询文章
    public Article findArticleById();
}
