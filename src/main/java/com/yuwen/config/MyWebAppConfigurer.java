package com.yuwen.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//配置虚拟路径，通过虚拟路径指向磁盘路径
@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {
    /*
        /image/upload/** 表示虚拟路径
        file:/D:/image/upload/ 表示磁盘路径
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/image/upload/**").addResourceLocations("file:/D:/image/upload/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }
}
