package com.yuwen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMarkdownDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMarkdownDemoApplication.class, args);
    }

}
